# Cryptocurrency Prices with Spring Boot

| Service Name | crypto-prices                                                                            |
| ------------ | ---------------------------------------------------------------------------------------- |
| Language     | ![](https://img.shields.io/badge/Java-v11-EB2024?logo=java&style=flat)                   |
| Requisites   | ![](https://img.shields.io/badge/PostgreSQL-v13-2F5E8D?logo=postgresql&style=flat)       |
| Service Host | Demo: [https://demo.andytang.dev/crypto-prices](https://demo.andytang.dev/crypto-prices) |

## Keywords

- Java: `Gradle`, `Spring Framework`, `JPA`, `Hibernate`, `Thymeleaf`
- Database: `PostgreSQL`, `RDBMS`
- DevOps: `Docker`, `Kubernetes`, `GitLab CI/CD`

## Overview

This service fetches data from [bitstamp.net](https://www.bitstamp.net) every 5 seconds and stores the result to database.

Users can access the following information of our selected cryptocurrencies with the provided APIs:

- latest market price
- market prices in last minute

We also provided a simple webpage to display the above information.

## REST API List

- [REST APIs](doc/rest-apis.md)
  - [Get All Cryptocurrencies Latest Prices](doc/rest-apis.md#get-all-cryptocurrencies-latest-prices)
  - [Get Last Minute Prices for Specific Cryptocurrency](doc/rest-apis.md#get-last-minute-prices-for-specific-cryptocurrency)

## API Response Format

This service is designed with reference to [Google API Design Guide](https://cloud.google.com/apis/design). All REST API response **must** contain at least one of the following top-level members:

- `data`: The reqested "primary data".
- `err`: Error object.
- `timestamp`: API response timestamp.

An error object contains the following members:

- `code`: Code to identity the error, [Response Status Codes](https://tools.ietf.org/html/rfc7231#section-6) from [RFC7231](https://tools.ietf.org/html/rfc7231) were mostly used.
- `msg`: Error message.
- `details`: Details of the error. (Only on debug mdoe)

## Quick start

Start application using Gradle:

```shell
./gradlew clean bootRun
```

Start application using Gradle with development profile:

```shell
SPRING_PROFILES_ACTIVE=dev ./gradlew clean bootRun
```
