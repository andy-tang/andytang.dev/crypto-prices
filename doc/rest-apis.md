# REST APIs

- [REST APIs](#rest-apis)
  - [Get All Cryptocurrencies Latest Prices](#get-all-cryptocurrencies-latest-prices)
    - [Description](#description)
    - [Example Request](#example-request)
    - [Example Response](#example-response)
  - [Get Last Minute Prices for Specific Cryptocurrency](#get-last-minute-prices-for-specific-cryptocurrency)
    - [Description](#description-1)
    - [Path Parameters](#path-parameters)
    - [Example Request](#example-request-1)
    - [Example Response](#example-response-1)

## Get All Cryptocurrencies Latest Prices

| GET | /api/prices |
| --- | ----------- |

### Description

Gets latest prices for all selected cryptocurrencies.

### Example Request

```shell
curl 'https://demo.andytang.dev/crypto-prices/api/prices'
```

### Example Response

```json
{
  "code": 200,
  "data": [
    {
      "timestamp": 1621847582,
      "crypto": "BTC",
      "rate": 36447.33
    },
    {
      "timestamp": 1621847583,
      "crypto": "ETH",
      "rate": 2271.62
    },
    {
      "timestamp": 1621847583,
      "crypto": "XRP",
      "rate": 0.84224
    }
  ],
  "timestamp": 1621847589
}
```

---

## Get Last Minute Prices for Specific Cryptocurrency

| GET | /api/prices/:crypto |
| --- | ------------------- |

### Description

Gets last-minute prices for the user inputted cryptocurrency.

### Path Parameters

| Field Name | Data Type | Required | Description     | Example |
| ---------- | --------- | -------- | --------------- | ------- |
| crypto     | string    | TRUE     | Cryptocurrency. | BTC     |

### Example Request

```shell
curl 'https://demo.andytang.dev/crypto-prices/api/prices/BTC'
```

### Example Response

```JSON
{
    "code": 200,
    "data": [
        {
            "timestamp": 1621848418,
            "crypto": "BTC",
            "rate": 36444.1
        },
        // ...
        {
            "timestamp": 1621848361,
            "crypto": "BTC",
            "rate": 36473.45
        }
    ],
    "timestamp": 1621848422
}
```
