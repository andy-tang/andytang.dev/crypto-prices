package dev.andytang.crypto.price.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "CRYPTO_PRICES", indexes = { @Index(columnList = "CRYPTO", name = "CRYPTO_IDX") })
@Data
@NoArgsConstructor
public class CryptoPrice implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private long id;

    @Column(name = "TIMESTAMP")
    private int timestamp;

    @Column(name = "CRYPTO")
    private String crypto;

    @Column(name = "RATE")
    private double rate;

    @Column(name = "CREATED_AT", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    @Generated(GenerationTime.INSERT)
    @JsonIgnore
    private LocalDateTime createdAt;

    public CryptoPrice(int timestamp, String crypto, double rate) {
        this.timestamp = timestamp;
        this.crypto = crypto;
        this.rate = rate;
    }
}
