package dev.andytang.crypto.price.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dev.andytang.crypto.price.model.CryptoPrice;

@Repository
@Transactional(readOnly = true)
public interface CryptoPriceRepository extends JpaRepository<CryptoPrice, Long> {
    List<CryptoPrice> findTop12ByCryptoOrderByTimestampDesc(String crypto);

    @Query(nativeQuery = true, value = "SELECT DISTINCT ON(crypto) * FROM crypto_prices WHERE crypto IN (:cryptos) ORDER BY crypto, timestamp DESC")
    List<CryptoPrice> findAllLatestByCrypto(@Param("cryptos") List<String> cryptos);

    @Modifying
    @Transactional
    public long deleteByCreatedAtBefore(LocalDateTime createdAt);
}