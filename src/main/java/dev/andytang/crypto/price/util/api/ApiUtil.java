package dev.andytang.crypto.price.util.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ApiUtil {
    public static <T> ResponseEntity<ResponseEnvelope<T>> response(HttpStatus status, T data) {
        return new ResponseEntity<>(new ResponseEnvelope<>(data), status);
    }

    public static <T> ResponseEntity<ResponseEnvelope<T>> response(HttpStatus status, String message, Throwable error) {
        return new ResponseEntity<>(new ResponseEnvelope<>(status.value(), message, error), status);
    }
}
