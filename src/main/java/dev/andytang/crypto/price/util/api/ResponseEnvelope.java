package dev.andytang.crypto.price.util.api;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
class Err {
    private int code;

    @JsonProperty("msg")
    private String message;

    private String details;

    public Err(int code, String message, Throwable err) {
        this.code = code;
        this.message = message;
        this.details = err.getMessage();
    }
}

@Getter
@JsonInclude(Include.NON_EMPTY)
public class ResponseEnvelope<T> {

    private long timestamp = Instant.now().getEpochSecond();
    private T data;
    private Err err;

    public ResponseEnvelope(T data) {
        this.data = data;
    }

    public ResponseEnvelope(int code, String message, Throwable error) {
        this.err = new Err(code, message, error);
    }
}
