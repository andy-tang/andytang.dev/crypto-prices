package dev.andytang.crypto.price.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dev.andytang.crypto.price.model.CryptoPrice;
import dev.andytang.crypto.price.repository.CryptoPriceRepository;
import dev.andytang.crypto.price.util.api.ApiUtil;
import dev.andytang.crypto.price.util.api.ResponseEnvelope;

@RestController
@RequestMapping("/api/prices")
public class PricesController {
    @Autowired
    CryptoPriceRepository cryptoPriceRepository;

    @Value("${dev.andytang.crypto.crypto-list}")
    private String[] cryptos;

    @GetMapping(value = { "", "/{crypto}" })
    public ResponseEntity<ResponseEnvelope<List<CryptoPrice>>> getPriceByCrypto(
            @PathVariable(required = false) String crypto) {
        List<CryptoPrice> cryptoPrices = new ArrayList<>();
        if (crypto == null) {
            cryptoPriceRepository.findAllLatestByCrypto(Arrays.asList(cryptos)).forEach(cryptoPrices::add);
        } else {
            cryptoPriceRepository.findTop12ByCryptoOrderByTimestampDesc(crypto).forEach(cryptoPrices::add);
        }

        return ApiUtil.response(HttpStatus.OK, cryptoPrices);
    }
}
