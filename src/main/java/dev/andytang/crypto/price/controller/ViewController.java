package dev.andytang.crypto.price.controller;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import dev.andytang.crypto.price.model.CryptoPrice;
import dev.andytang.crypto.price.repository.CryptoPriceRepository;

@Controller
public class ViewController {
    @Autowired
    CryptoPriceRepository cryptoPriceRepository;

    @Value("${dev.andytang.crypto.crypto-list}")
    private String[] cryptos;

    @GetMapping("/")
    public String index(Model model) {
        List<CryptoPrice> cryptoPrices = new ArrayList<>();
        cryptoPriceRepository.findAllLatestByCrypto(Arrays.asList(cryptos)).forEach(cryptoPrices::add);
        model.addAttribute("cryptoPrices", cryptoPrices);
        model.addAttribute("timestamp", Instant.now());
        return "index";
    }
}
