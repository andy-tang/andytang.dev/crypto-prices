package dev.andytang.crypto.price.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.NoHandlerFoundException;

import dev.andytang.crypto.price.util.api.ApiUtil;
import dev.andytang.crypto.price.util.api.ResponseEnvelope;

@ControllerAdvice
public class ExceptionController {
    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseBody
    public ResponseEntity<ResponseEnvelope<String>> requestHandlingNoHandlerFound() {
        return ApiUtil.response(HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND.getReasonPhrase(), new Exception());
    }
}