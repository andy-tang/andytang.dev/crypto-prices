package dev.andytang.crypto.price.service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import dev.andytang.crypto.price.model.CryptoPrice;
import dev.andytang.crypto.price.repository.CryptoPriceRepository;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
class BitStampResponse {
    String high;
    String last;
    String timestamp;
    String bid;
    String vwap;
    String volume;
    String low;
    String ask;
    String open;
}

class CallBitStampApi implements Callable<CryptoPrice> {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_1_1)
            .connectTimeout(Duration.ofSeconds(10)).build();

    private String crypto;

    CallBitStampApi(String crypto) {
        this.crypto = crypto;
    }

    @Override
    public CryptoPrice call() throws IOException, InterruptedException {
        // Get latest price from bitstamp
        var request = HttpRequest.newBuilder().GET()
                .uri(URI.create("https://www.bitstamp.net/api/v2/ticker/" + crypto.toLowerCase() + "usd")).build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        var bitStampResponse = objectMapper.readValue(response.body(), BitStampResponse.class);

        // Return CryptoPrice object
        return new CryptoPrice(Integer.valueOf(bitStampResponse.timestamp), crypto,
                Double.valueOf(bitStampResponse.last));

    }

}

@Slf4j
@Component
public class CryptoPriceService {
    private static ExecutorService executorService = Executors.newFixedThreadPool(10);
    private static List<Callable<CryptoPrice>> callableTasks = new ArrayList<>();

    @Autowired
    private CryptoPriceRepository cryptoPriceRepository;

    @Value("${dev.andytang.crypto.crypto-list}")
    private String[] cryptos;

    @PostConstruct
    private void postConstruct() {
        for (String crypto : cryptos) {
            callableTasks.add(new CallBitStampApi(crypto));
        }
    }

    @Scheduled(cron = "*/5 * * * * *")
    public void fetchCryptoPrices() {
        List<Future<CryptoPrice>> futures = new ArrayList<>();
        try {
            futures = executorService.invokeAll(callableTasks, 5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        List<CryptoPrice> cryptoPrices = new ArrayList<>();
        for (Future<CryptoPrice> future : futures) {
            try {
                cryptoPrices.add(future.get());
            } catch (CancellationException | ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

        cryptoPriceRepository.saveAll(cryptoPrices);
        log.info("Fetched data: " + cryptoPrices);
    }

    @Scheduled(cron = "0 0 * * * *")
    public void deleteOutdatedRecords() {
        var deleted = cryptoPriceRepository.deleteByCreatedAtBefore(LocalDateTime.now().minusHours(1));
        log.info("Outdated records removed: " + deleted);
    }
}
