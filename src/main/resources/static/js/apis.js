document.addEventListener('DOMContentLoaded', () => {
  const apis = [
    {
      title: 'Get All Cryptocurrencies Latest Prices',
      endpoint: {
        method: 'GET',
        path: '/api/prices',
      },
      desc: 'Gets latest prices for all selected cryptocurrencies.',
      params: [],
      req: "curl 'https://demo.andytang.dev/crypto-prices/api/prices'",
      test: async () => {
        const respElm = document.getElementById(
          this.getAttribute('data-target')
        )
        if (!!!respElm) return

        respElm.innerHTML = 'loading...'
        const resp = await fetch(
          'https://demo.andytang.dev/crypto-prices/api/prices'
        ).then((response) => {
          return response.json()
        })

        respElm.innerHTML = JSON.stringify(resp, null, 2)
        hljs.highlightBlock(respElm)
      },
    },
    {
      title: 'Get Last Minute Prices for Specific Cryptocurrency',
      endpoint: {
        method: 'GET',
        path: '/api/prices/:crypto',
      },
      desc: 'Gets last-minute prices for the user inputted cryptocurrency.',
      params: [
        {
          name: 'crypto',
          type: 'string',
          required: true,
          desc: 'Cryptocurrency.',
          example: 'BTC',
        },
      ],
      req: "curl 'https://demo.andytang.dev/crypto-prices/api/prices/BTC'",
      test: async () => {
        const respElm = document.getElementById(
          this.getAttribute('data-target')
        )
        if (!!!respElm) return

        respElm.innerHTML = 'loading...'
        const resp = await fetch(
          'https://demo.andytang.dev/crypto-prices/api/prices/BTC'
        ).then((response) => {
          return response.json()
        })

        respElm.innerHTML = JSON.stringify(resp, null, 2)
        hljs.highlightBlock(respElm)
      },
    },
  ]

  apis.forEach((api, index) => {
    const elm = document.createElement('div')
    elm.innerHTML = `
    <hr />
    <div class="api">
      <h5>${api.title}</h5>
      <div class="end-point">
        <div class="method">${api.endpoint.method}</div>
        <div class="path">${api.endpoint.path}</div>
      </div>
      <h5>Description</h5>
      <p>${api.desc}</p>
      ${
        api.params.length > 0
          ? `
            <h5>Parameters</h5>
            <table style="width:100%">
              <tr>
                <th>Field Name</th>
                <th>Data Type</th>
                <th>Required</th>
                <th>Description</th>
                <th>Example</th>
              </tr>
              ${api.params.map(
                (param) =>
                  `
                  <tr>
                    <td>${param.name}</td>
                    <td>${param.type}</td>
                    <td>${param.required}</td>
                    <td>${param.desc}</td>
                    <td>${param.example}</td>
                  </tr>
                `
              )}
              </table>
            `
          : ''
      }
      <h5>Example Request</h5>
      <pre>
        <code class="hljs bash">${api.req}</code>
      </pre>
      <h5>Example Response</h5>
      <div class="d-flex justify-content-end">
        <button type="button" onclick="(${api.test?.toString()})()" data-target="api-resp-${index}" class="btn btn-primary btn-sm">Test API</button>
      </div>
      <pre>
        <code id="api-resp-${index}" class="hljs json">{}</code>
      </pre>
    </div>
    `
    document.getElementsByClassName('apis')?.[0]?.appendChild(elm)
  })
})
