# syntax = docker/dockerfile:experimental
#--------------------------------------------------------------------------------
# Stage 1: Compile Java codes
#--------------------------------------------------------------------------------
FROM openjdk:11-slim-buster as builder
LABEL stage=intermediate

WORKDIR /opt/app
COPY . .
RUN --mount=type=cache,target=/root/.gradle ./gradlew clean build -x test

#--------------------------------------------------------------------------------
# Stage 2: Prepare running environment for Java application
#--------------------------------------------------------------------------------
FROM openjdk:11-slim-buster

COPY --from=builder /opt/app/build/libs/*.jar /opt/app/app.jar
EXPOSE 8090
CMD ["java", "-jar", "/opt/app/app.jar"]
